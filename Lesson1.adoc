= Lesson 1 - Introduction to Computation
Mani Kamali
kamalisarvestani.1@themetroschool.org

== Idealized Components 

=== Defining Abstract [\~ 5 min]
What does it mean for something to be abstract?
    For example, mechanical can point to a gear and associate it with the letters 'g e a r'. On the other hand, we can not point at an if-statement or other logic componenet the way we can a gear. You can somewhat simulate the experience by observing how the if-statement interacts on an isolated level. But the deeper you go the harder this gets, for example, it is far more difficult to define the plus sign (+) in this way. (Try to imagine explaining the plus sign in this way).

=== Components in Programming [\~ 5 min]

    "In computer science we deal with idealized components, that is to say:
    You see, when an engineer is designing a physical system, that's made out of real parts. The engineers who worry about that have to address problems of tolerance and approximation and noise in the system. So for example, as an electrical engineer, I can go off and easily build a one-stage amplifier or a two-stage amplifier, and I can imagine cascading a lot of them to build a million-stage amplifier. But it's ridiculous to build such a thing, because long before the millionth stage, the thermal noise in those components way at the beginning is going to get amplified and make the whole thing meaningless. Computer science deals with idealized components. We know as much as we want about these little program and data pieces that we're fitting things together. We don't have to worry about tolerance. And that means that, in building a large program, there's not all that much difference between what I can build and what I can imagine, because the parts are these abstract entities that I know as much as I want." - Gerald Jay Sussman MIT 1986

== Introduction to Programming (Languages) [\~ 5-10 min]

There are three main things you should look for in a language when looking programming-wise. 
    1) Primitives - These are the base units of the language. For example the number '3' (where the symbol 3 is really just a representation of the abstract concept of the number three), can be a primitive. Just as well the text '"Hello"' can be a primitive. In some languages these might be treated the same or they might be treated differently. 
    2) Means of combination - Ways of combining primitives, for example 2 + 3 combines two primitives (2 and 3) into a new 'combination' (5). Or you could have '"Hello " + "bob"' which 'combines' into "Hello bob".
    3) Means of abstraction - These are tools to build abstractions on existing primitives. Think of this as defining 'your own' primitives. 

These three measures should be the main ways you rate and define languages. Syntax should take a backseat to these in most cases. That being said we, as a team that recently switched to kotlin, did so mainly because of syntax reasons. In our specific instance Kotlin provides nothing but benefits as a language (in some things such as Objects, which enhance the means of abstraction) as well as syntax benefits. 

I'd also like to note that anything I wrote here (for example "Hello " + "Bob") will not interact the same for every language. They might not interact the way I claimed here in ANY language. The idea is to get you a rough understanding of how this works.



